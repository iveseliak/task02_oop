package com.example.oop.company;

import com.example.oop.aircraft.Aircraft;
import com.example.oop.aircraft.CargoAircraft;
import com.example.oop.aircraft.PassengerAircraft;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Company {
    private String name;
    private List<Aircraft> aircrafts;

    public Company(String name, List<Aircraft> aircrafts) {
        this.name = name;
        this.aircrafts = aircrafts;
    }

    public int getPassengerСapacity() {
        return aircrafts.stream().filter(s -> s instanceof PassengerAircraft)
                .mapToInt(aircraft -> ((PassengerAircraft) aircraft).getPassengerСapacity()).sum();
    }

    public int getLoadCarryingCapacity() {
        return aircrafts.stream().filter(s -> s instanceof CargoAircraft)
                .mapToInt(aircraft -> ((CargoAircraft) aircraft).getLoadCarryingCapacity()).sum();

    }

    public List<Aircraft> getAircraftsWhereFuelConsumptionBetweenParams(int min, int max) {
        return aircrafts.stream()
                .filter(s -> s.getFuelConsumption() >= min && s.getFuelConsumption() <= max)
                .collect(Collectors.toList());
    }

}
