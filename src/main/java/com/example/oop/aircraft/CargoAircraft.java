package com.example.oop.aircraft;

public class CargoAircraft extends Aircraft {

    private int loadCarryingCapacity;

    public CargoAircraft(String name, int flightRange, int fuelConsumption, int loadCarryingCapacity) {
        this.name = name;
        this.flightRange = flightRange;
        this.fuelConsumption = fuelConsumption;
        this.loadCarryingCapacity = loadCarryingCapacity;
    }


    public void takeoff() {
        System.out.println("Cargo aircraft took off");
    }

    public void landing() {
        System.out.println("The cargo plane landed");
    }

    public int getLoadCarryingCapacity() {
        return loadCarryingCapacity;
    }

    public void setLoadCarryingCapacity(int loadCarryingCapacity) {
        this.loadCarryingCapacity = loadCarryingCapacity;
    }


    @Override
    public String toString() {
        return "CargoAircraft{" +
                "loadCarryingCapacity=" + loadCarryingCapacity +
                ", name='" + name + '\'' +
                ", flightRange=" + flightRange +
                ", fuelConsumption=" + fuelConsumption +
                '}';
    }
}
