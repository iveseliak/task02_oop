package com.example.oop.aircraft;

public abstract class Aircraft {
    public String name;
    public int flightRange;
    public int fuelConsumption;

    public Aircraft() {
        this.flightRange = flightRange;
        this.fuelConsumption = fuelConsumption;
    }

    public abstract void takeoff();

    public abstract void landing();

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "name='" + name + '\'' +
                ", flightRange=" + flightRange +
                ", fuelConsumption=" + fuelConsumption +
                '}';
    }
}
