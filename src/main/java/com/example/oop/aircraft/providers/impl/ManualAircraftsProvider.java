package com.example.oop.aircraft.providers.impl;

import com.example.oop.aircraft.Aircraft;
import com.example.oop.aircraft.CargoAircraft;
import com.example.oop.aircraft.PassengerAircraft;
import com.example.oop.aircraft.providers.AircraftProvider;

import java.util.ArrayList;
import java.util.List;

public class ManualAircraftsProvider implements AircraftProvider {
    public List<Aircraft> getAircrafts() {
        List<Aircraft> aircrafts = new ArrayList<Aircraft>();
        aircrafts.add(new CargoAircraft("Ан-12", 5530, 381, 20));
        aircrafts.add(new CargoAircraft("Ан-22", 6400, 250, 16));
        aircrafts.add(new CargoAircraft("Ан-124", 8000, 450, 56));
        aircrafts.add(new PassengerAircraft("Ан-12", 4600, 210, 130));
        aircrafts.add(new PassengerAircraft("Ан-140", 5600, 305, 160));
        aircrafts.add(new PassengerAircraft("Airbus A318", 5700, 268, 132));
        aircrafts.add(new CargoAircraft("Boeing 707", 6200, 284, 150));
        return aircrafts;
    }
}
