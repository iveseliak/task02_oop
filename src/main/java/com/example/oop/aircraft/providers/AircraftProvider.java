package com.example.oop.aircraft.providers;

import com.example.oop.aircraft.Aircraft;

import java.util.List;

public interface AircraftProvider {

    List<Aircraft> getAircrafts();
}
