package com.example.oop.aircraft;

public class PassengerAircraft extends Aircraft {

    private int passengerСapacity;

    public PassengerAircraft(String name, int flightRange, int fuelConsumption, int passengerСapacity) {
        this.name = name;
        this.flightRange = flightRange;
        this.fuelConsumption = fuelConsumption;
        this.passengerСapacity = passengerСapacity;
    }

    public void takeoff() {
        System.out.println("Passenger aircraft took off");
    }

    public void landing() {
        System.out.println("The passenger plane landed");
    }

    public int getPassengerСapacity() {
        return passengerСapacity;
    }

    public void setPassengerСapacity(int passengerСapacity) {
        this.passengerСapacity = passengerСapacity;
    }

    @Override
    public String toString() {
        return "PassengerAircraft{" +
                "passengerСapacity=" + passengerСapacity +
                ", name='" + name + '\'' +
                ", flightRange=" + flightRange +
                ", fuelConsumption=" + fuelConsumption +
                '}';
    }
}
