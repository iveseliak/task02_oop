package com.example.oop.main;

import com.example.oop.aircraft.providers.AircraftProvider;
import com.example.oop.aircraft.providers.impl.ManualAircraftsProvider;
import com.example.oop.company.Company;

public class Main {

    public static void main(String[] args) {
        AircraftProvider provider=new ManualAircraftsProvider();

        Company company=new Company("Airlines", provider.getAircrafts());



        System.out.println("Загальна пасажиромісткість: "+company.getPassengerСapacity());

        System.out.println("Загальна грузопідйомність: "+company.getLoadCarryingCapacity());

        System.out.println("Літаки, які відповідають заданому діапазону споживання палива: "+company.getAircraftsWhereFuelConsumptionBetweenParams(250,300));


    }
}
